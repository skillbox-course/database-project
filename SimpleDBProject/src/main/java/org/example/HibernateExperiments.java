package org.example;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.example.dbo.Course;
import org.example.dbo.CourseType;
import org.example.dbo.Student;
import org.example.dbo.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Slf4j
public class HibernateExperiments {
    @Getter
    private static SessionFactory sessionFactory;

    public HibernateExperiments() {
        initSessionFactory();
    }

    public static void initSessionFactory(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        sessionFactory = metadata.getSessionFactoryBuilder().build();
    }

    public Course getCourseById(int id){
        Session session = sessionFactory.openSession();
        return session.get(Course.class, id);
    }

    public Teacher getTeacherById(int id){
        Session session = sessionFactory.openSession();
        return session.get(Teacher.class, id);
    }

    public Teacher getTeacherOfCourse(int courseId){
        Session session = sessionFactory.openSession();
        Course course = session.get(Course.class, courseId);
        return course.getTeacher();
    }

    public void createCourse(String name, CourseType type, Teacher teacher){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Course course = new Course();
        course.setName(name);
        course.setType(type);
        course.setTeacher(teacher);
        session.save(course);
        transaction.commit();
        log.info("new course added");
    }

    public void updateCourseName(String name, int courseId){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Course course = session.get(Course.class, courseId);
        course.setName(name);
        session.save(course);
        transaction.commit();
        log.info("course name updated");
    }

    public void deleteCourseById(int courseId){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Course course = session.get(Course.class, courseId);
        session.delete(course);
        transaction.commit();
        log.info("course deleted");
    }

    public List<Student> getStudentsOfCourse(int courseId){
        Session session = sessionFactory.openSession();
        Course course = session.get(Course.class, courseId);
        return course.getStudents();
    }

    public List<Course> getCoursesUsingCriteriaQuery(){
        log.info("select courses with price >= 100_000 using criteria query:");
        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Course> queryCourse = builder.createQuery(Course.class);
        Root<Course> root = queryCourse.from(Course.class);

        queryCourse.select(root)
                .where(builder.greaterThanOrEqualTo(root.get("price"), 100_000))
                .orderBy(builder.desc(root.get("price")));
        return session.createQuery(queryCourse).setMaxResults(10).getResultList();
    }

    public List<Course> getCoursesUsingHQL(){
        log.info("select courses with price >= 100_000 using HQL:");
        Session session = sessionFactory.openSession();
        String hqlQuery = "From " + Course.class.getSimpleName() + " where price >= 100000";
        return session.createQuery(hqlQuery).getResultList();
    }

    // TODO Напишите код, который выведет среднее количество покупок в месяц
    //  для каждого курса за 2018 год.
    //  Учитывайте диапазон месяцев, в течение которых были продажи.
    public void outputCourseAndSales(){
        getCoursesAndSalesUsingSQL()
                .forEach(objects -> {
                    String courseName = objects[0].toString();
                    Double numberOfCourseSold = Double.parseDouble(objects[1].toString());
                    Double numberOfSalesMonth = Double.parseDouble(objects[2].toString());
                    Double avgSalesPerMonth = numberOfCourseSold / numberOfSalesMonth;
                    System.out.println(courseName + " - " + String.format("%.2f", avgSalesPerMonth));
                });
    }
    public List<Object[]> getCoursesAndSalesUsingSQL(){
        Session session = sessionFactory.openSession();
        String query = "select course_name, count(course_name), max(month(subscription_date)) " +
                "from PurchaseList group by course_name ";
        return session.createNativeQuery(query).getResultList();
    }
}
