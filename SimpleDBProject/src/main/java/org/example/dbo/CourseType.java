package org.example.dbo;

public enum CourseType {
    DESIGN,
    PROGRAMMING,
    MARKETING,
    MANAGEMENT,
    BUSINESS
}
