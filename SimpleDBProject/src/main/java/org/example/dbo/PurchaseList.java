package org.example.dbo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
public class PurchaseList implements Serializable {
    @Id
    @Column(name = "student_name")
    private String studentName;
    @Id
    @Column(name = "course_name")
    private String courseName;
    private int price;
    @Column(name = "subscription_date")
    private Date subscriptionDate;

    public PurchaseList(String courseName, Date subscriptionDate) {
        this.courseName = courseName;
        this.subscriptionDate = subscriptionDate;
    }

    @Override
    public String toString() {
        return studentName + " - " +
                courseName + " - " +
                price + " - " +
                subscriptionDate;
    }
}
