package org.example;

public class Main {

    public static void main(String[] args) {
        HibernateExperiments hibernateExperiments = new HibernateExperiments();

//        Course course17 = hibernateExperiments.getCourseById(17);
//        Teacher teacher = hibernateExperiments.getTeacherOfCourse(17);
//        System.out.println(teacher.getName() + " - преподаватель курса " + course17.getName());
//        hibernateExperiments.createCourse("new course", CourseType.DESIGN, teacher);
//        hibernateExperiments.updateCourseName("very new course", 52);
//        hibernateExperiments.deleteCourseById(52);
//        hibernateExperiments.getStudentsOfCourse(course17.getId())
//                .forEach(student -> System.out.println(student.getName()));
//        hibernateExperiments.getCoursesUsingCriteriaQuery()
//                .forEach(course -> System.out.println(course.getName() + " - " + course.getPrice()));
//        hibernateExperiments.getCoursesUsingHQL()
//                .forEach(course -> System.out.println(course.getName() + " - " + course.getPrice()));

        hibernateExperiments.outputCourseAndSales();

        HibernateExperiments.getSessionFactory().close();
    }
}