package org.example;

public class Start_SimpleDB {
    public static void main(String[] args) {

        DBTools dbTools = new DBTools();
        dbTools.generateLinkedPurchaseList();

        DBTools.getSessionFactory().close();
    }
}