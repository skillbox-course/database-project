package org.example;

import lombok.Getter;
import org.example.dao.Course;
import org.example.dao.LinkedPurchaseList;
import org.example.dao.PurchaseList;
import org.example.dao.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class DBTools {
    @Getter
    private static SessionFactory sessionFactory;

    public DBTools() {
        initSessionFactory();
    }

    public static void initSessionFactory(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        sessionFactory = metadata.getSessionFactoryBuilder().build();
    }

    // getting data from PurchaseList. Then from PurchaseList data getting student and course
    // and filling in the table LinkedPurchaseList
    public void generateLinkedPurchaseList(){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        CriteriaBuilder builder = session.getCriteriaBuilder();

        CriteriaQuery<PurchaseList> queryPurchase = builder.createQuery(PurchaseList.class);
        queryPurchase.from(PurchaseList.class);
        List<PurchaseList> purchaseList = session.createQuery(queryPurchase).getResultList();
        CriteriaQuery<Student> queryStudent = builder.createQuery(Student.class);
        Root<Student> rootStudent = queryStudent.from(Student.class);
        CriteriaQuery<Course> queryCourse = builder.createQuery(Course.class);
        Root<Course> rootCourse = queryCourse.from(Course.class);

        purchaseList.forEach(purchase -> {
            queryStudent.where(builder.equal(rootStudent.get("name"), purchase.getStudentName()));
            Student student = session.createQuery(queryStudent).getSingleResult();
            queryCourse.where(builder.equal(rootCourse.get("name"), purchase.getCourseName()));
            Course course = session.createQuery(queryCourse).getSingleResult();
            session.save(new LinkedPurchaseList(student.getId(), course.getId()));
        });

        transaction.commit();
    }
}
