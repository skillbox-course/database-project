package org.example.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity(name = "Subscriptions")
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Subscription implements Serializable {
    @Id
    @ManyToOne(cascade = CascadeType.ALL)
    private Student student;
    @Id
    @ManyToOne(cascade = CascadeType.ALL)
    private Course course;
    @Column(name = "subscription_date")
    private Date subscriptionDate;
}
