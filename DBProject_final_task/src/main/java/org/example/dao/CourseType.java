package org.example.dao;

public enum CourseType {
    DESIGN,
    PROGRAMMING,
    MARKETING,
    MANAGEMENT,
    BUSINESS
}
