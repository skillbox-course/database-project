package org.example.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity(name = "PurchaseList")
@IdClass(PKPurchaseList.class)
public class PurchaseList {
    @Id
    @Column(name = "student_name")
    private String studentName;
    @Id
    @Column(name = "course_name")
    private String courseName;
    private int price;
    @Column(name = "subscription_date")
    private Date subscriptionDate;

    public PurchaseList(String studentName, String courseName) {
        this.studentName = studentName;
        this.courseName = courseName;
    }
}
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Data
class PKPurchaseList implements Serializable {
    private String studentName;
    private String courseName;
}