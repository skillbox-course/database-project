package org.example.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity(name = "LinkedPurchaseList")
public class LinkedPurchaseList {
    @EmbeddedId
    private PKLinkedPurchaseList id;
    @Column(name = "student_id", insertable = false, updatable = false)
    private int studentId;
    @Column(name = "course_id", insertable = false, updatable = false)
    private int courseId;

    public LinkedPurchaseList(int studentId, int courseId) {
        this.id = new PKLinkedPurchaseList(studentId, courseId);
        this.studentId = studentId;
        this.courseId = courseId;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode
    @Data
    class PKLinkedPurchaseList implements Serializable {
        @Column(name = "student_id")
        private int studentId;
        @Column(name = "course_id")
        private int courseId;
    }

}


